const Operations = require("./node_modules/calculatorLibrary/calculator");

const myCalc = new Operations();

console.log("ADD: ", myCalc.Add(123, 45));
console.log("SUBTRACT: ", myCalc.Subtract(123, 45));
console.log("MULTIPLY: ", myCalc.Multiply(123, 45));
console.log("DIVIDE: ", myCalc.Divide(123, 45));