var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
const minify = require('gulp-minify');

/*gulp.task('compress', function (cb) {
    pump([
          gulp.src('lib/*.js'),
          uglify(),
          gulp.dest('dist')
      ],
      cb
    );
});*/

gulp.task('compress', function() {
    gulp.src(['lib/*.js', 'lib/*.mjs'])
      .pipe(minify())
      .pipe(gulp.dest('dist'))
});

const gulp = require('gulp');
const zip = require('gulp-zip');
 
gulp.task('default', () =>
    gulp.src('dist/*')
        .pipe(zip('dist-min.zip'))
        .pipe(gulp.dest('dist'))
);